#!/usr/bin/env python3
"""Lab 13 challange: print() and input()"""    

def main():

    # This information will print users name to function
    user_name = input("Please enter your name: ")
    
    # Asking user for day of the week 
    day_of_week = input("Please input day of the week: ")
    
    # This will print name and day of the week
    greeting = f"Hello, {user_name}! Happy {day_of_week}!"
    print(greeting)

main()

