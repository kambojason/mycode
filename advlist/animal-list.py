#!/usr/bin/env python3

#Optional Challenge

# Animal List
animals = ["Fox", "Fly", "Ant", "Bee", "Cod", "Cat", "Dog", "Yak", "Cow", "Hen", "Koi", "Hog", "Jay", "Kit"]

# print the list the original way
print(animals)

# print list without any markings only the words

for item in animals:
    print(item)

# now print list on the same line without markings
for item in animals:
    print(item, end=" ")
    
# add an additional line thats empty
print()

