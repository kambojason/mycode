#!/usr/env/python3

# All things needed for code
mylist= [1,2,3,4,5, "Python"]

# This will be the day of week
day = mylist[1]

# This will be Python
the_class = mylist[5]

# Get users name
name= input("What is your name? ")

# Capitilize all characters in name
name_upper = name.upper()

# This is what you should see when print runs-
# Hi <name>! Welcome to Day 2 of Python Training!
phrase = f"Hi {name_upper}! Welcome to Day {day} of {the_class} Training!"
print(phrase)

